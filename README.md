```
   ___ _                __    __           _    _                     _     
  / _ \ |_   _  ___ ___/ / /\ \ \___  _ __| | _| |__   ___ _ __   ___| |__  
 / /_\/ | | | |/ __/ _ \ \/  \/ / _ \| '__| |/ / '_ \ / _ \ '_ \ / __| '_ \ 
/ /_\\| | |_| | (_| (_) \  /\  / (_) | |  |   <| |_) |  __/ | | | (__| | | |
\____/|_|\__, |\___\___/ \/  \/ \___/|_|  |_|\_\_.__/ \___|_| |_|\___|_| |_|
         |___/                                                              
```

# Build

```
mvn clean package
```

# Run

```
java -XstartOnFirstThread -jar ./target/glycoworkbench2-0.0.2-SNAPSHOT-jar-with-dependencies.jar
```

# Version

* GWB_MAJOR.GWB_MINOR GWB_STATE (Build: GWB_BUILD)

# Developers

* Masaaki Mastubara, Issaku Yamada

* (Previous version: Alessio Ceroni, Kai Maass and David Damerell)

# Contact

* yamadaissaku@gmail.com

# Web site

* TODO:

# Installation

## Windows users

* There are two ways to install this software:

1. Download and run the installer
2. Download the Windows specific Zip file and extract

## All other users
Download your platform specific Zip file and extract 

# Run

## Windows users

* The run options will depend on the installation method chosen (see above)

1. If you installed using the designated installer then you should find both
a Desktop Icon and a Start bar entry for GlycoWorkBench, click on either to
launch the program

2. If you opted for the Windows specific Zip file then launch by either clicking on the executable or the jar file

## All other useres

* GlycoWorkBench can be launched by double clicking the executable jar or app file

# Troubleshooting

## Run

* If the above methods failed to launch program then attempt to manually launch as follows:

1. Open a terminal or command prompt
2. Navigate to the directory that contains GWB
3. Run java -jar -Xmx300M eurocarb-glycoworkbench-1.0rc.jar
If the above method fails to launch then email the given error message to yamadaissaku@gmail.com

* If you can launch the application from the jar file but your shortcuts/executables don't work try the following:

1. Go to Start/Control panel
2. Select Programs/Uninstall a program
3. Within the pop-up window search for Java(TM)6(major version)Update 20(minor version)
4. If your version of Java has listed beside it (64bit) then you must uninstall this version and install the 32bit equivalent
Unfortunately there is no available open source solution to this issue at this time 

# Manual

* The GlycoWorkbench (GWB) manual can be accessed via the Help menu of GWB.  



# Bugs/Comments

* If you have discovered a bug or have a suggestion please submit a bug report to the gitlab repository
 - https://gitlab.com/glycoinfo/glycoworkbench
  
# Requirements

* Java 8 (or later)

* maven 3.6 (or later)

http://java.sun.com/javase/downloads/index.jsp

GlycoWorkbench >= 1.3.0, will only run on systems that support the SWT library (includes; Windows, Linux and macOS)
